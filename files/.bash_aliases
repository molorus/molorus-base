alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias lsl='ls -alF'
alias lslt='ls -alFtrh'
alias vi='/usr/bin/vim'
function ape2flac () { 
	FILENAME=$( echo "$1" | sed -e 's/^Ce/flac/')
	echo "Converting $FILENAME from ape to flac...."
	NEW="${FILENAME}.flac"
	avconv -i "$1" "$NEW" 
	}

function flac2mp3 () {
	NEW="${1/%flac/mp3}"
	echo "Converting $FILENAME from flac to mp3...."
  	echo $NEW

	ARTIST=$(metaflac "$1" --show-tag=ARTIST | sed s/.*=//g)
	TITLE=$(metaflac "$1" --show-tag=TITLE | sed s/.*=//g)
	ALBUM=$(metaflac "$1" --show-tag=ALBUM | sed s/.*=//g)
	GENRE=$(metaflac "$1" --show-tag=GENRE | sed s/.*=//g)
	TRACKNUMBER=$(metaflac "$1" --show-tag=TRACKNUMBER | sed s/.*=//g)
	DATE=$(metaflac "$1" --show-tag=DATE | sed s/.*=//g)
	ffmpeg -i "$1" -qscale:a 0 "$NEW"
}
function wav2mp3 () { 
	echo "Converting $1 from wav to mp3...."
	lame -V 0 -h "$1" 
	}
