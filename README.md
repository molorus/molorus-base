# base

#### Table of Contents

1. [Description](#description)

## Description

This is the base module for home.molorus.com Linux hosts. You probably won't find much use.

base::init.pp - installs ntpdate, adds daily cronjob if on a physical CentOS host (TODO: support 'buntu/debian)

base::jpgfiles.pp - sets .profile, .bash profile/resources/aliases, .vimrc

base::repos.pp - sets up repos and yum-priorities (TODO: support 'buntu/debian)