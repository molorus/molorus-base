# Class: base::jpgfiles
# ===========================
#
#  In order to set up jpg's home on CentOS servers,
#  we set a few system properties
#  0. - ensure account is present
#  1. - add .ssh/authorized_keys
#  2. - add .bashrc, .bashaliases, etc
#  3. - add .vimrc
#  4. - add ./bin/ (added to $PATH in step2
#
# Parameters
# ----------
# This module accepts no parameters.
#
# Variables
# ----------
#
# Examples
# --------
#
# @example
#
# Authors
# -------
#
# John Garvin jpg@molorus.com
#
# Copyright
# ---------
#
# Copyright 2016 John Garvin, unless otherwise noted.
#
class base::jpgfiles {
    notify { 'beginning jpgfiles module':

        }
    notify { 'ensure ~/bin/':
        message => 'creating/ensuring ~/bin/',
        }
    file { '/home/jpg/bin':
        ensure => 'directory',
        owner => 'jpg',
        group => 'jpg',
        mode => '0750',
        path => '/home/jpg/bin',
        }
    notify { 'ensure ~/.ssh/':
        message => 'creating/ensuring ~/.ssh/',
        }
    file { '/home/jpg/.ssh':
        ensure => 'directory',
        owner => 'jpg',
        group => 'jpg',
        mode => '0700',
        path => '/home/jpg/.ssh',
        }
    notify { 'ensure ~/.ssh/authorized_keys':
        message => 'creating/ensuring ~/.ssh/authorized_keys',
        }
    file { '/home/jpg/.ssh/authorized_keys':
        ensure => 'file',
        owner => 'jpg',
        group => 'jpg',
        mode => '0600',
        path => '/home/jpg/.ssh/authorized_keys',
        source => 'puppet:///modules/base/authorized_keys',
        }
    notify { 'ensure ~/.profile':
        message => 'creating/ensuring ~/.profile',
        }
    file { '/home/jpg/.profile':
        ensure => 'file',
        owner => 'jpg',
        group => 'jpg',
        mode => '0700',
        path => '/home/jpg/.profile',
        source => 'puppet:///modules/base/.profile',
        }
    notify { 'ensure ~/.bash_profile':
        message => 'creating/ensuring ~/.bash_profile',
        }
    file { '/home/jpg/.bash_profile':
        ensure => 'file',
        owner => 'jpg',
        group => 'jpg',
        mode => '0700',
        path => '/home/jpg/.bash_profile',
        source => 'puppet:///modules/base/.bash_profile',
        }
    notify { 'ensure ~/.bashrc':
        message => 'creating/ensuring ~/.bashrc',
        }
    file { '/home/jpg/.bashrc':
        ensure => 'file',
        owner => 'jpg',
        group => 'jpg',
        mode => '0700',
        path => '/home/jpg/.bashrc',
        source => 'puppet:///modules/base/.bashrc',
        }
    notify { 'ensure ~/.bash_aliases':
        message => 'creating/ensuring ~/.bash_aliases',
        }
    file { '/home/jpg/.bash_aliases':
        ensure => 'file',
        owner => 'jpg',
        group => 'jpg',
        mode => '0700',
        path => '/home/jpg/.bash_aliases',
        source => 'puppet:///modules/base/.bash_aliases',
        }
    notify { 'ensure ~/.toprc':
        message => 'creating/ensuring ~/.toprc',
        }
    file { '/home/jpg/.toprc':
        ensure => 'file',
        owner => 'jpg',
        group => 'jpg',
        mode => '0700',
        path => '/home/jpg/.toprc',
        source => 'puppet:///modules/base/.toprc',
        }
    notify { 'ensure ~/.vimrc':
        message => 'creating/ensuring ~/.vimrc',
        }
    file { '/home/jpg/.vimrc':
        ensure => 'file',
        owner => 'jpg',
        group => 'jpg',
        mode => '0750',
        path => '/home/jpg/.vimrc',
        source => 'puppet:///modules/base/.vimrc',
        }
}
