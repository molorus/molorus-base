# Class: base::repos
# ===========================
#
#  In order to set up yum repos on CentOS servers,
#  we set a few system properties
#  0. - enable microserver CentOS repo
#  1. - enable yum-priorities yum plugin
#  2. - set priorities of each repo (actually, copy /etc/yum.repos.d/*repo files)
#
# Parameters
# ----------
# This module accepts no parameters.
#
# Variables
# ----------
#
# Examples
# --------
#
# @example
#
# Authors
# -------
#
# John Garvin jpg@molorus.com
#
# Copyright
# ---------
#
# Copyright 2016 John Garvin, unless otherwise noted.
#
class base::repos {
    if $operatingsystem == 'CentOS'  {
    notify { 'CentOS detected, beginning repos module':
        message => 'beginning repo configuration',
        }
    notify { 'installing package yum-plugin-priorities':
        message => 'installing yum-plugin-priorities' ,
        }
    package {'yum-plugin-priorities': 
        name => 'yum-plugin-priorities',
        ensure => 'installed',
        }
    notify { 'adding localrepo':
        message => 'adding localrepo',
        }
    #yumrepo {'localrepo': 
    #    name => 'localrepo',
    #    ensure => 'present',
    #    baseurl => 'http://bigass.home.molorus.com/repo/centos',
    #    enabled => 'yes',
    #    gpgcheck => 'no',
    #    priority => '1',
    #    }
    # perhaps it's stupid to manage other repos via the file resource vs
    # the yumrepo resource type
    # TODO: convert?
    notify { 'adding official CentOS repos':
        message => 'adding official CentOS repos',
        }
    file { '/etc/yum.repos.d/CentOS-Base.repo':
        ensure => 'present',
        owner => 'root',
        group => 'root',
        mode => '0644',
        path => '/etc/yum.repos.d/CentOS-Base.repo',
        source => 'puppet:///modules/base/CentOS-Base.repo',
        }
    file { '/etc/yum.repos.d/CentOS-CR.repo':
        ensure => 'present',
        owner => 'root',
        group => 'root',
        mode => '0644',
        path => '/etc/yum.repos.d/CentOS-CR.repo',
        source => 'puppet:///modules/base/CentOS-CR.repo',
        }
    file { '/etc/yum.repos.d/CentOS-fasttrack.repo':
        ensure => 'present',
        owner => 'root',
        group => 'root',
        mode => '0644',
        path => '/etc/yum.repos.d/CentOS-fasttrack.repo',
        source => 'puppet:///modules/base/CentOS-fasttrack.repo',
        }
    file { '/etc/yum.repos.d/CentOS-Debuginfo.repo':
        ensure => 'present',
        owner => 'root',
        group => 'root',
        mode => '0644',
        path => '/etc/yum.repos.d/CentOS-Debuginfo.repo',
        source => 'puppet:///modules/base/CentOS-Debuginfo.repo',
        }
    file { '/etc/yum.repos.d/CentOS-Vault.repo':
        ensure => 'present',
        owner => 'root',
        group => 'root',
        mode => '0644',
        path => '/etc/yum.repos.d/CentOS-Vault.repo',
        source => 'puppet:///modules/base/CentOS-Vault.repo',
        }
    notify { 'Adding EPEL repo':
        message => 'Adding EPEL repo',
        }
    file { 'epel.repo':
        ensure => 'present',
        owner => 'root',
        group => 'root',
        mode => '0644',
        path => '/etc/yum.repos.d/epel.repo',
        source => 'puppet:///modules/base/epel.repo',
        }
    file { 'epel-testing.repo':
        ensure => 'present',
        owner => 'root',
        group => 'root',
        mode => '0644',
        path => '/etc/yum.repos.d/epel-testing.repo',
        source => 'puppet:///modules/base/epel-testing.repo',
        }
    file { 'localrepo.repo':
        ensure => 'present',
        owner => 'root',
        group => 'root',
        mode => '0644',
        path => '/etc/yum.repos.d/localrepo.repo',
        source => 'puppet:///modules/base/localrepo.repo',
        }
    }
}
