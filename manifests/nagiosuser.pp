# Class: base
# ===========================
#
#  In order to set up nagios clients 
#  we install a nagios user. All checks will be done by check_ssh from util1,
#  the nagios host (vm on bigass).
#
# Parameters
# ----------
# This module accepts no parameters.
#
# Variables
# ----------
#
# Examples
# --------
#
# @example
#    class { 'nagiosuser':
#    }
#
# Authors
# -------
#
# John Garvin jpg@molorus.com
#
# Copyright
# ---------
#
# Copyright 2016 John Garvin, unless otherwise noted.
#
class base::nagiosuser {
    notify { 'ensuring nagios plugins are installed': 
        message => 'ensuring nagios plugins are installed',
        }
    if $operatingsystem == 'CentOS' { 
        package { 'nagios-plugins-all':
            ensure => 'present',
            }
    }
    else { 
        package { 'nagios-plugins-standard': 
            ensure => 'present',
            }
        package { 'nagios-plugins-common': 
            ensure => 'present',
            }
        package { 'nagios-plugins-contrib': 
            ensure => 'present',
            }
          }
    notify { 'creating nagiosuser': 
        message => 'creaging nagiosuser',
        }
    accounts::user { 'nagios':
        ensure  => 'present',
        comment => 'nagios client',
        home    => '/home/nagiosuser',
        uid     => '1109',
        gid     => '1109',
        password => '!!',
        shell => '/bin/bash',
        sshkeys => [ 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC3EwAnXXq7irPYEu4ZMvQ2GDB1+fQTN+kUZD0c49b0HcJQ0k5coxTo6yNMPwt3g59T0dPRoaQGJSzNBIBL+uJ+P81WfvUOY29JuCDvQPOYmz70PpjwgLtmElNxHqVSly6VSMvApNUv7So1cM8l56wD6WAalyYAj1R8pE/TLghiZc9bxnNUpJ+i4NIOsJVT1QatPkSTYDFQL+0p41oix9YhUaxoyW5bDLoWi1o3dFzSS0s8An0ZWSMRnszH8PO6RzKLPRvxGzCUgRMJm4EREwE5mAuTZ7s6DrVny8st54eDKmgg/fZjqOYv1JJNE/s0NPeqPvUCcoIdl2AyqV29av5f nagios@util1' ],
        }
}
