# Class: base
# ===========================
#
#  In order to set up KVM guests on downstairs servers,
#  we set a few system properties
#  0. - enable microserver CentOS repo
#  1. - 
#
# Parameters
# ----------
# This module accepts no parameters.
#
# Variables
# ----------
#
# Examples
# --------
#
# @example
#    class { 'base':
#    }
#
# Authors
# -------
#
# John Garvin jpg@molorus.com
#
# Copyright
# ---------
#
# Copyright 2016 John Garvin, unless otherwise noted.
#
class base {
    notify { 'operatingsystem detected':
        message         => $operatingsystem ,
        }
    notify { 'installing vim': 
        message         => 'installing vim',
        }
    if $operatingsystem == 'CentOS' { 
        package {'vim': 
            name        => 'vim-minimal',
            ensure      => 'installed',
            }
        }
    elsif $operatingsystem ==  /^(Debian|Ubuntu)$/ {
        package {'vim': 
            name        => 'vim',
            ensure      => 'installed',
            }
        }
    if $operatingsystem == 'CentOS' { 
        notify { 'installing applydeltarpms package':
            message     => 'installing applydeltarpms package',
            }
        package {'deltarpm': 
            name        => 'deltarpm',
            ensure      => 'installed',
            }
        }
    if $virtual == 'physical' { 
        cron { 'ntpdate cronjob': 
            name        => 'daily ntpdate cronjob',
            ensure      => present,
            hour        => '6',
            minute      => '1',
            month       => '*',
            monthday    => '*',
            weekday     => '*',
            user        => 'root',
            command     => '/usr/sbin/ntpdate tock.usno.navy.mil',
            } 
        notify { 'installing ntpdate package':
            message     => 'installing ntpdate package',
            }
        package {'ntpdate': 
            name        => 'ntpdate',
            ensure      => 'installed',
            }
        }
    # misc packages
    package { 'htop':
            name        => 'htop',
            ensure      => 'installed',
        }
}
